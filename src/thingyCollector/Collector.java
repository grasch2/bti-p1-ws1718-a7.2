package thingyCollector;

import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import _untouchable_.thingy.Item;

public class Collector implements Collector_I {

    private List<Item> requested, memory;

    Collector() {
        requested = new ArrayList<Item>();
        memory = new ArrayList<Item>();
    }

    @Override
    public Collection<Item> process(Item item) {

        if (requested.contains(item)) {
            memory.add(item);
        } else {
            requested.add(item);
        }
        if (requested.size() > 4) {
            List<Item> result = requested; // einmal merken
            requested = new ArrayList<Item>(); // neue liste
            for (Item memorized : memory) {
                if (!requested.contains(memorized)) {
                    // requested wird mit den Items von memory befuellt,
                    // aber nicht mit Doppelte Items
                    requested.add(memorized);
                }
            }
            memory.removeAll(requested);
            return result;
        }
        return null;
    }// Methode Ende

    @Override
    public void reset() {
        requested.clear();
        memory.clear();
    }

    /**
     * @return Gibt die Laenge der Liste "requested" zurueck.
     */
    public int sizeOfRequested() {
        return requested.size();
    }

    /**
     * @return Gibt die Laenge der Liste "memory" zurueck.
     */
    public int sizeOfMemory() {
        return memory.size();
    }
}
